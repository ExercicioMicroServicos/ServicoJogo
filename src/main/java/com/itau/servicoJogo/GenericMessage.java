package com.itau.servicoJogo;

import java.lang.reflect.Field;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.itau.servicoJogo.model.NomeContratoMQ;

public class GenericMessage<T> {
	private T t;
	private String endereco;
	private String fila;
	
	public GenericMessage(T t, String endereco, String fila) {
		this.t= t;
		this.endereco = endereco;
		this.fila = fila;
	}
	
	private MapMessage toMapMessage(MapMessage body) throws IllegalArgumentException, IllegalAccessException {

		try {
			
			Field[] fields = t.getClass().getDeclaredFields();

			for (Field field : fields) {
				
				NomeContratoMQ annotation = field.getAnnotation(NomeContratoMQ.class);
				
				body.setString(annotation.valor() , field.get(t).toString());
			}
			
		} catch (JMSException ex)  {
			ex.printStackTrace();
		}
		return body;
		
	}
	
	public void sendMessage() {
		try {
			// Abrir conexão com o broker
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(endereco);
        	Connection connection = connectionFactory.createConnection();
        	connection.start();
        	
        	// Criar uma sessão
        	Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        	
        	// A fila que queremos popular
        	Queue queue = session.createQueue(fila); 
        	
        	// Nosso produtor de eventos
        	MessageProducer producer = session.createProducer(queue);
        	
      
        	MapMessage body = session.createMapMessage();
        	
        	body = toMapMessage(body);
        	
    		producer.send(body);
    		
		
		} catch(JMSException | IllegalAccessException ex) {
			ex.printStackTrace();
		}   

	}
}
