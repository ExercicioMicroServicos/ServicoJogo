package com.itau.servicoJogo;

import java.util.Map;

public interface Trackable {
	/**
	 * Retorna o tipo desse objeto (player, game, 
	 * @return
	 */
	public String getTrackedType();
	
	/**
	 * 
	 * @return
	 */
	public Map<String, String> getTrackedProperties();
}
