package com.itau.servicoJogo.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.servicoJogo.model.JogoPergunta;

public interface JogoPerguntaRepository extends CrudRepository<JogoPergunta, Long> {

}
