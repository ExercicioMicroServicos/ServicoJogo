package com.itau.servicoJogo.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.servicoJogo.model.Jogo;

public interface JogoRepository extends CrudRepository<Jogo, Long> {

}
