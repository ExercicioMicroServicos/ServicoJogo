package com.itau.servicoJogo.services;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itau.servicoJogo.RPCClient;
import com.itau.servicoJogo.model.Questao;

@Service
public class QuestaoService {
	
	private final String URL = "http://10/162.109.139:8080";
	private RestTemplate restTemplate = new RestTemplate();
	
	public Questao criarPergunta(Questao questao) {

		String url = URL + String.format("/questao");
		ResponseEntity<Questao> response = restTemplate.exchange(url, HttpMethod.POST,new HttpEntity<Questao>(questao), Questao.class);

		return response.getBody();
	}
	
			
	public Questao buscarPergunta(long id) {
		
		String url = URL + String.format("/questao/id=%o", id);
		ResponseEntity<Questao> response = restTemplate.exchange(url, HttpMethod.GET,null, Questao.class);

		return response.getBody();
	}
	
	
	public Questao[] buscarPerguntas() {
		
		String url = URL + String.format("/questoes");
		ResponseEntity<Questao[]> response = restTemplate.exchange(url, HttpMethod.GET,null, Questao[].class);

		return response.getBody();
	}
	
	public Questao[] gerarPerguntas(int quantidade) {
		
//		String url = URL + String.format("/questoes/quantidade=%o", quantidade);
//		ResponseEntity<Questao[]> response = restTemplate.exchange(url, HttpMethod.GET,null, Questao[].class);
//
//		return response.getBody();
		RPCClient rpcClient = new RPCClient();
		
		return rpcClient.send(quantidade);
		
	}
	
	public Questao alterarPergunta(long id,Questao questao) {
		
		String url = URL + String.format("/questao/id=%o", id);
		ResponseEntity<Questao> response = restTemplate.exchange(url, HttpMethod.PUT,new HttpEntity<Questao>(questao), Questao.class);

		return response.getBody();
	}
	
	public Questao deletarPergunta(long id) {
		
		String url = URL + String.format("/questao/id=%o", id);
		ResponseEntity<Questao> response = restTemplate.exchange(url, HttpMethod.PUT,null, Questao.class);

		return response.getBody();
	}

}
