package com.itau.servicoJogo.services;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itau.servicoJogo.Sender;
import com.itau.servicoJogo.model.Pontuacao;
import com.itau.servicoJogo.model.Questao;

@Service
public class PontuacaoService {
	
	public void salvarPontuacao(Pontuacao pontuacao) {
		new Sender().sendMessage(pontuacao);
	}
	
}
