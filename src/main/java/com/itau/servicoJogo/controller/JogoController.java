package com.itau.servicoJogo.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.servicoJogo.Event;
import com.itau.servicoJogo.Events;
import com.itau.servicoJogo.LogKafka;
import com.itau.servicoJogo.model.Jogo;
import com.itau.servicoJogo.model.JogoPergunta;
import com.itau.servicoJogo.model.JogoResposta;
import com.itau.servicoJogo.model.Pontuacao;
import com.itau.servicoJogo.model.Questao;
import com.itau.servicoJogo.model.Resposta;
import com.itau.servicoJogo.repositories.JogoPerguntaRepository;
import com.itau.servicoJogo.repositories.JogoRepository;
import com.itau.servicoJogo.services.QuestaoService;

import ch.qos.logback.classic.util.LogbackMDCAdapter;

import com.itau.servicoJogo.services.PontuacaoService;

@RestController
@CrossOrigin
public class JogoController {
	
	@Autowired
	JogoRepository jogoRepository;
	
	@Autowired
	JogoPerguntaRepository jogoPerguntaRepository;
	
	@Autowired
	PontuacaoService pontuacaoService;
	
	@Autowired
	QuestaoService perguntaService;
	
	@RequestMapping(method=RequestMethod.POST, path="/jogo")
	public Jogo criarJogo(@Valid @RequestBody Jogo jogo) {
		Jogo novoJogo = jogoRepository.save(jogo);
		
		Events.emmit("game", "Jogo Iniciado", novoJogo);
//		LogKafka log = new LogKafka("game", "GrupoE Jogo Iniciado id: " + novoJogo.getId());
//		Thread t = log.createProducerThread(1);
//		t.start();
//		
//		Questao[] questoes = perguntaService.gerarPerguntas(novoJogo.getNumeroQuestoes());
//			
//		for(Questao q : questoes) {
//			JogoPergunta jogoPergunta  = new JogoPergunta(novoJogo.getId(),q.getId());
//			jogoPerguntaRepository.save(jogoPergunta);
//			
//			log = new LogKafka("questions", "GrupoE Gerar Perguntas: " + q.getId());
//			Thread t1 = log.createProducerThread(2);
//			t1.start();
//		}


		Pontuacao pont = new Pontuacao();
		pont.nomeJogador = Long.toString(jogo.getIdJogador());
		pont.acertos = 0;
		pont.erros = 0;
		pont.idJogo = jogo.getId();
		pont.pontuacao = 0;

		pontuacaoService.salvarPontuacao(pont); 
		
		return novoJogo;
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/jogos")
	public Iterable<Jogo> buscarJogos(){
		return jogoRepository.findAll();
	}
		
	@RequestMapping(method=RequestMethod.GET, path="/jogo/{id}")
	public ResponseEntity<?> buscarJogo(@PathVariable long id){
		
		Optional<Jogo> jogoOptional = jogoRepository.findById(id);
		
		if(!jogoOptional.isPresent()) {
			return  ResponseEntity.notFound().build();
		}
		
		return  ResponseEntity.ok().body(jogoOptional.get());
	}
	
	
	@RequestMapping(method=RequestMethod.PUT, path="/jogo/{id}")
	public ResponseEntity<?> alterarJogo(@Valid @PathVariable long id, @RequestBody Jogo jogo) {
		Optional<Jogo> jogoOptional = jogoRepository.findById(id);
		
		if(!jogoOptional.isPresent()) {
			return  ResponseEntity.notFound().build();
		}
		
		Jogo jogoBanco = jogoOptional.get();
		jogoBanco.setNumeroQuestoes(jogo.getNumeroQuestoes());
		jogoBanco.setAcertos(jogo.getAcertos());
		jogoBanco.setErros(jogo.getErros());
		jogoBanco.setQuestaoAtual(jogo.getQuestaoAtual());
		jogoBanco.setIdJogador(jogo.getIdJogador());

		Jogo jogoSalva = jogoRepository.save(jogo);
		
		return  ResponseEntity.ok().body(jogoSalva);	
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/jogo/{id}")
	public ResponseEntity<?> deletarJogo(@PathVariable long id){
		
		Optional<Jogo> jogoOptional = jogoRepository.findById(id);
		
		if(!jogoOptional.isPresent()) {
			return  ResponseEntity.notFound().build();
		}
		
		jogoRepository.deleteById(id);
		
		return  ResponseEntity.ok().build();
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/jogo/validar")
	public ResponseEntity<?> verificarJogo(@Valid @RequestBody JogoResposta jogoResposta) {
		
		Questao questao = perguntaService.buscarPergunta(jogoResposta.getIdPergunta());
		
		Optional<Jogo> jogoOptional = jogoRepository.findById(jogoResposta.getIdJogo());
		Jogo jogoBanco = jogoOptional.get();
		boolean acertou = false;
		Jogo j = new Jogo();
;		
		for(Resposta r : questao.getRespostas()) {
			if(r.getId() == jogoResposta.getIdResposta() && r.isCorreta()) {
				j.setAcertos(jogoBanco.getAcertos() + 1);
				j.setQuestaoAtual(jogoResposta.getIdPergunta());
				acertou = true;
			}
			
		}
		
		if(!acertou) {
			j.setErros(jogoBanco.getErros() + 1);
		}
		
		Jogo jogoSalva = jogoRepository.save(j);
		
		return  ResponseEntity.ok().body(jogoSalva);
	}
	
}
