package com.itau.servicoJogo.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.servicoJogo.model.JogoPergunta;
import com.itau.servicoJogo.repositories.JogoPerguntaRepository;

@RestController
@CrossOrigin
public class JogoPerguntaController {
	
	@Autowired
	JogoPerguntaRepository jogoPerguntaRepository;
	
	@RequestMapping(method=RequestMethod.POST, path="/jogo/pergunta")
	public JogoPergunta criarJogoPergunta(@Valid @RequestBody JogoPergunta jogoPergunta) {
		return jogoPerguntaRepository.save(jogoPergunta);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/jogo/perguntas")
	public Iterable<JogoPergunta> buscarJogoPerguntas(){
		return jogoPerguntaRepository.findAll();
	}
		
	@RequestMapping(method=RequestMethod.GET, path="/jogo/pergunta/{id}")
	public ResponseEntity<?> buscarJogoPergunta(@PathVariable long id){
		
		Optional<JogoPergunta> jogoPerguntaOptional = jogoPerguntaRepository.findById(id);
		
		if(!jogoPerguntaOptional.isPresent()) {
			return  ResponseEntity.notFound().build();
		}
		
		return  ResponseEntity.ok().body(jogoPerguntaOptional.get());
	}
	
	
	@RequestMapping(method=RequestMethod.PUT, path="/jogo/pergunta/{id}")
	public ResponseEntity<?> alterarJogoPergunta(@Valid @PathVariable long id, @RequestBody JogoPergunta jogoPergunta) {
		Optional<JogoPergunta> jogoPerguntaOptional = jogoPerguntaRepository.findById(id);
		
		if(!jogoPerguntaOptional.isPresent()) {
			return  ResponseEntity.notFound().build();
		}
		
		JogoPergunta jogoPerguntaBanco = jogoPerguntaOptional.get();
		jogoPerguntaBanco.setIdPergunta(jogoPergunta.getIdJogoPergunta());
		jogoPerguntaBanco.setIdJogo(jogoPergunta.getIdJogo());
		
		JogoPergunta jogoPerguntaSalva = jogoPerguntaRepository.save(jogoPergunta);
		
		return  ResponseEntity.ok().body(jogoPerguntaSalva);	
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/jogo/pergunta/{id}")
	public ResponseEntity<?> deletarJogoPergunta(@PathVariable long id){
		
		Optional<JogoPergunta> jogoPerguntaOptional = jogoPerguntaRepository.findById(id);
		
		if(!jogoPerguntaOptional.isPresent()) {
			return  ResponseEntity.notFound().build();
		}
		
		jogoPerguntaRepository.deleteById(id);
		
		return  ResponseEntity.ok().build();
	}

}
