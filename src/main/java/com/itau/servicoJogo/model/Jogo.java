package com.itau.servicoJogo.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.itau.servicoJogo.Trackable;

@Entity
public class Jogo implements Trackable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	private int numeroQuestoes;
	
	private int idJogador;
	
	private long questaoAtual;
	
	private int acertos;
	
	private int erros;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNumeroQuestoes() {
		return numeroQuestoes;
	}

	public void setNumeroQuestoes(int numeroQuestoes) {
		this.numeroQuestoes = numeroQuestoes;
	}

	public int getAcertos() {
		return acertos;
	}

	public void setAcertos(int acertos) {
		this.acertos = acertos;
	}

	public int getErros() {
		return erros;
	}

	public void setErros(int erros) {
		this.erros = erros;
	}

	public long getQuestaoAtual() {
		return questaoAtual;
	}

	public void setQuestaoAtual(long questaoAtual) {
		this.questaoAtual = questaoAtual;
	}

	public int getIdJogador() {
		return idJogador;
	}

	public void setIdJogador(int idJogador) {
		this.idJogador = idJogador;
	}

	public String getTrackedType() {
		return Jogo.class.getSimpleName();
	}
	
	public Map<String, String> getTrackedProperties() {
		Map<String, String>  map = new HashMap<String, String>();
		map.put("id", String.valueOf(id));
		map.put("idJogador", String.valueOf(idJogador));
		map.put("acertos", String.valueOf(acertos));
		map.put("erro", String.valueOf(erros));
		return map;
	}
	
}
