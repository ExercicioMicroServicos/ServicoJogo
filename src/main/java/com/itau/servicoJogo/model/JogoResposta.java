package com.itau.servicoJogo.model;

public class JogoResposta  {
	
	private long idJogador;
	
	private long idJogo;
	
	private long idPergunta;
	
	private int idResposta;
	
	public long getIdJogador() {
		return idJogador;
	}

	public void setIdJogador(long idJogador) {
		this.idJogador = idJogador;
	}

	public long getIdJogo() {
		return idJogo;
	}

	public void setIdJogo(long idJogo) {
		this.idJogo = idJogo;
	}

	public long getIdPergunta() {
		return idPergunta;
	}

	public void setIdPergunta(long idPergunta) {
		this.idPergunta = idPergunta;
	}

	public int getIdResposta() {
		return idResposta;
	}

	public void setIdResposta(int idResposta) {
		this.idResposta = idResposta;
	}
	

}
