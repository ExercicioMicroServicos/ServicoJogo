package com.itau.servicoJogo.model;

import java.util.List;

public class Questao {
	
	private long id;
	
	private String pergunta;
	
	private List<Resposta> respostas;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getPergunta() {
		return pergunta;
	}
	
	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}
	
	public List<Resposta> getRespostas() {
		return respostas;
	}
	
	public void setRespostas(List<Resposta> resposta) {
		this.respostas = resposta;
	}
	
}
