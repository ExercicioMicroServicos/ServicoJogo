package com.itau.servicoJogo.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.itau.servicoJogo.Trackable;

@Entity
public class JogoPergunta implements Trackable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idJogoPergunta;
	
	@NotNull
	private long idJogo;
	
	@NotNull
	private long idPergunta;
	
	public JogoPergunta(long idJogo,long idPergunta) {
		setIdJogo(idJogo);
		setIdPergunta(idPergunta);
	}
	

	public long getIdJogoPergunta() {
		return idJogoPergunta;
	}

	public void setIdJogoPergunta(long idJogoPergunta) {
		this.idJogoPergunta = idJogoPergunta;
	}

	public long getIdJogo() {
		return idJogo;
	}

	public void setIdJogo(long idJogo) {
		this.idJogo = idJogo;
	}

	public long getIdPergunta() {
		return idPergunta;
	}

	public void setIdPergunta(long idPergunta) {
		this.idPergunta = idPergunta;
	}


	public String getTrackedType() {
		return JogoPergunta.class.getName();		
	}


	public Map<String, String> getTrackedProperties() {
		Map<String, String>  map = new HashMap<String, String>();
		map.put("idJogo", String.valueOf(idJogo));
		map.put("idJogoPergunta", String.valueOf(idJogoPergunta));
		map.put("idPergunta", String.valueOf(idPergunta));
		return map;
	}
	

}
