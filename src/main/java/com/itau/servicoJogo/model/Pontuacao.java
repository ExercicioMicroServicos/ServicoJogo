package com.itau.servicoJogo.model;

import java.lang.annotation.Annotation;

public class Pontuacao implements NomeContratoMQ{

	@NomeContratoMQ(valor="gameId")
	public Long idJogo;
	@NomeContratoMQ(valor="playerName")
	public String nomeJogador;
	@NomeContratoMQ(valor="total")
	public int pontuacao;
	@NomeContratoMQ(valor="hits")
	public int acertos;
	@NomeContratoMQ(valor="misses")
	public int erros;

}
