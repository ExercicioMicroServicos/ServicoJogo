package com.itau.servicoJogo;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.itau.servicoJogo.model.Pontuacao;


public class Sender {
	String endereco = "tcp://amq.oramosmarcos.com:61616";
	String fila = "e.queue.ranking";
	
	public void sendMessage(Pontuacao pont) {
		
		GenericMessage<Pontuacao> mensagem = new GenericMessage<Pontuacao>(pont, endereco, fila);
		
		mensagem.sendMessage();

	}
	
	
	
}
