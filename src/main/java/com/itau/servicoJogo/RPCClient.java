package com.itau.servicoJogo;

import java.util.ArrayList;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.itau.servicoJogo.model.Questao;

public class RPCClient {

	private static String QUEUE_NAME = "e.queue.questions.id";
	private static String AMQ_SERVER = "tcp://amq.oramosmarcos.com:61616";

	public Questao[] send (int quantidade) {
		Questao[] questoes = null;
		try {
			// Abrir conexão com o broker
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(AMQ_SERVER);
			Connection connection = connectionFactory.createConnection();
			connection.start();

			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue queue = session.createQueue(QUEUE_NAME);
			MessageProducer producer = session.createProducer(queue);
			Destination tempQueue = session.createTemporaryQueue();
			MessageConsumer responseConsumer = session.createConsumer(tempQueue);
			
			MapMessage msg = session.createMapMessage();
			msg.setString("quantity", String.valueOf(quantidade));
			msg.setJMSCorrelationID("val-req-" + quantidade);
			msg.setJMSReplyTo(tempQueue);
			
			// Envia a mensagem
			producer.send(msg);

			MapMessage response = (MapMessage) responseConsumer.receive(100000);
			
			ArrayList objList = (ArrayList)response.getObject("ids");
			
			questoes = new Questao[objList.size()];
			
			for (int i = 0; i < objList.size(); i++) {
				Questao questao = new Questao();

				questao.setId((Long)objList.get(i));
				questoes[i] = questao;
			}
			
			responseConsumer.close();

		} catch (JMSException ex) {
			ex.printStackTrace();
		}
		return questoes;
	}

}
