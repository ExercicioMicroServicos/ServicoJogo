package com.itau.servicoJogo;

public class SimpleLogEngine extends LogEngine{

	@Override
	public void log(String message) {
		LogKafka log = new LogKafka("game", message);
		Thread t = log.createProducerThread(1);
		t.start();
	}
}
